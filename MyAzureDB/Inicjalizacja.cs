﻿using Microsoft.Azure.Cosmos.Table;
using MyAzureDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAzureDB
{
    public class Inicjalizacja : TableEntity
    {

        public void CreateTable(CloudTableClient tableClient, string tableName)
        {
            try
            {
                CloudTable table = tableClient.GetTableReference(tableName);
                if (table.CreateIfNotExists())
                {
                    Console.WriteLine("Created Table named: {0}", tableName);
                }
                else
                {
                    Console.WriteLine("Table {0} already exists", tableName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
            }
        }

        public void InsertTableOferta(CloudTableClient tableClient, Oferta entity)
        {
            CloudTable table = tableClient.GetTableReference("Oferta");
            if (entity == null)
            {
                throw new ArgumentNullException("oferta");
            }
            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge((ITableEntity)entity);
                TableResult result = table.Execute(insertOrMergeOperation);
            }
            catch (Exception e)
            {
                Console.WriteLine("WARNING: " + e.Message);
            }
        }
        public void InsertTableKlient(CloudTableClient tableClient, Klient entity)
        {
            CloudTable table = tableClient.GetTableReference("Klient");
            if (entity == null)
            {
                throw new ArgumentNullException("klient");
            }
            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge((ITableEntity)entity);
                TableResult result = table.Execute(insertOrMergeOperation);
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
            }
        }

        public void DropTables(CloudTableClient tableClient, string tableName)
        {
            CloudTable table = tableClient.GetTableReference(tableName);
            if (table.DeleteIfExists())
            {
                Console.WriteLine("Drop Table named: {0}", tableName);
            }
            else
            {
                Console.WriteLine("Table {0} already exists", tableName);
            }
        }
    }
}
