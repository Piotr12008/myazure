﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAzureDB.Entity
{
    public class OfertaKlient : TableEntity
    {
        public string IdOferta { get; set; }
        public string IdKlient { get; set; }
        public DateTime Rozpoczecie { get; set; }
        public DateTime Zakonczenie { get; set; }

        public OfertaKlient(string id, string idOferta, string idKlient, DateTime rozpoczecie, DateTime zakonczenie)
        {
            PartitionKey = id;
            RowKey = id;
            this.IdOferta = idOferta;
            this.IdKlient = idKlient;
            this.Rozpoczecie = rozpoczecie;
            this.Zakonczenie = zakonczenie;
        }

        public OfertaKlient() { }
    }
}
