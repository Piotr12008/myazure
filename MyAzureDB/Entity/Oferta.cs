﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAzureDB.Entity
{
    public class Oferta : TableEntity
    {
        public String Nazwa { get; set; }
        public String Panstwo { get; set; }
        public String Miejscowosc { get; set; }
        public int DniPobytu { get; set; }
        public int LiczbaOfert { get; set; }

        public Oferta(string id, String nazwa, String panstwo, String miejscowosc, int dniPobytu, int liczbaOfert)
        {
            PartitionKey = id;
            RowKey = id;
            this.Nazwa = nazwa;
            this.Panstwo = panstwo;
            this.Miejscowosc = miejscowosc;
            this.DniPobytu = dniPobytu;
            this.LiczbaOfert = liczbaOfert;
        }
        public Oferta() { }
    }
}
