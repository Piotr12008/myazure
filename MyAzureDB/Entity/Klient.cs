﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAzureDB.Entity
{
    public class Klient : TableEntity
    {
        public String Imie { get; set; }
        public String Nazwisko { get; set; }

        public Klient()
        {
        }

        public Klient(String id, String imie, String nazwisko)
        {
            PartitionKey = id;
            RowKey = id;
            this.Imie = imie;
            this.Nazwisko = nazwisko;
        }
    }
}
