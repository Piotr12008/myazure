﻿using MyAzureDB.Entity;
using System;
using System.Collections.Generic;
using Microsoft.Azure.Cosmos.Table;
using MyAzureDB.Repository;

namespace MyAzureDB
{
    class Program
    {
        public static void MenuGlowne()
        {
            Console.WriteLine("MENU GŁÓWNE");
            Console.WriteLine("1. Oferty");
            Console.WriteLine("2. Klienci");
            Console.WriteLine("3. Obsługa klienta");
            Console.WriteLine("4. Usuń tabele");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuOferta()
        {
            Console.WriteLine("MENU Oferty");
            Console.WriteLine("1. Dodaj");
            Console.WriteLine("2. Pokaż wszystkie");
            Console.WriteLine("3. Usuń");
            Console.WriteLine("4. Szukaj");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuKlient()
        {
            Console.WriteLine("MENU Klient");
            Console.WriteLine("1. Dodaj");
            Console.WriteLine("2. Pokaż wszystkie");
            Console.WriteLine("3. Edytuj");
            Console.WriteLine("4. Usuń");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuWypozyczenia()
        {
            Console.WriteLine("MENU Obsługi klienta");
            Console.WriteLine("1. Sprzedaj ofertę");
            Console.WriteLine("2. Historia ofert klienta");
            Console.WriteLine("3. Dostępne oferty");
            Console.WriteLine("q. Wyjście");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Łączenie...");
            
            string connectionString = "DefaultEndpointsProtocol=https;AccountName=psrlab6;AccountKey=nbmSPZB8MVhtDbYT7T39xbTqBLKeAa4h8UWHSo3lOnEqMi9X9QiTlJIpzTIfJ9rawiIN8SmaD7w8h19Rembi8w==;TableEndpoint=https://psrlab6.table.cosmos.azure.com:443/;";
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            Inicjalizacja inicjalizacja = new Inicjalizacja();

            inicjalizacja.DropTables(tableClient, "Oferta");
            inicjalizacja.DropTables(tableClient, "Klient");
            inicjalizacja.DropTables(tableClient, "OfertaKlient");

            Console.WriteLine("Tworzenie tabel i wstępnych danych...");
            inicjalizacja.CreateTable(tableClient,"Oferta");
            inicjalizacja.CreateTable(tableClient,"Klient");
            inicjalizacja.CreateTable(tableClient,"OfertaKlient");

            Oferta oferta1 = new Oferta(Guid.NewGuid().ToString(), "Italy All Inclusive", "Włochy", "Rzym", 5, 2);
            inicjalizacja.InsertTableOferta(tableClient, oferta1);
            Oferta oferta2 = new Oferta(Guid.NewGuid().ToString(), "France All Inclusive", "Francja", "Paryż", 10, 2);
            inicjalizacja.InsertTableOferta(tableClient, oferta2);

            Klient klient1 = new Klient(Guid.NewGuid().ToString(), "Michał", "Kowal");
            inicjalizacja.InsertTableKlient(tableClient, klient1);
            Klient klient2 = new Klient(Guid.NewGuid().ToString(), "Karol", "Pawlik");
            inicjalizacja.InsertTableKlient(tableClient, klient2);

            Console.WriteLine("Zakończono inicjalizację.");
            Console.ReadKey();

            ConsoleKey key = new ConsoleKey();
            KlientRepo klientRepo = new KlientRepo(tableClient);
            OfertaRepo ofertaRepo = new OfertaRepo(tableClient);
            OfertaKlientRepo ofertaKlientRepo = new OfertaKlientRepo(tableClient);

            while (true)
            {
                MenuGlowne();
                key = Console.ReadKey().Key;
                if (key.Equals(ConsoleKey.D1))
                {
                    while (true)
                    {
                        MenuOferta();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj nazwe: ");
                            String nazwa = Console.ReadLine();
                            Console.WriteLine("Podaj Państwo: ");
                            String panstwo = Console.ReadLine();
                            Console.WriteLine("Podaj Miejscowosc: ");
                            String miejscowosc = Console.ReadLine();
                            Console.WriteLine("Podaj Ilość dni pobytu: ");
                            String dni = Console.ReadLine();
                            Console.WriteLine("Podaj liczbe ofert: ");
                            String liczbaOfert = Console.ReadLine();
                            Oferta oferta = new Oferta(Guid.NewGuid().ToString(),nazwa, panstwo, miejscowosc, int.Parse(dni), int.Parse(liczbaOfert));
                            if (ofertaRepo.Save(oferta) == true)
                                Console.WriteLine("Zapisano !");
                            else
                                Console.WriteLine("Błąd zapisu");
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            List<Oferta> oferty = ofertaRepo.FindAll();
                            foreach (Oferta oferta in oferty)
                            {
                                string dostepna = "";
                                if (oferta.LiczbaOfert > 0)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(oferta.RowKey + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + " - " + oferta.DniPobytu.ToString() + " (" + dostepna + ") " + oferta.LiczbaOfert);
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            Console.WriteLine("Podaj ID oferty do usuniecia: ");
                            List<Oferta> oferty = ofertaRepo.FindAll();
                            foreach (Oferta oferta in oferty)
                            {
                                string dostepna = "";
                                if (oferta.LiczbaOfert > 0)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(oferta.RowKey + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + "-" + oferta.DniPobytu + " (" + dostepna + ")");
                            }
                            String id = Console.ReadLine();
                            if (ofertaRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                if (ofertaRepo.Remove(id) == true)
                                    Console.WriteLine("Usunięto !");
                                else
                                    Console.WriteLine("Błąd usuwania");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D4))
                        {
                            Console.WriteLine("Podaj Państwo: ");
                            String panstwo = Console.ReadLine();
                            List<Oferta> oferty = new List<Oferta>();
                            oferty = ofertaRepo.FindAll(panstwo);

                            if (oferty.Count != 0)
                            {
                                foreach (Oferta oferta in oferty)
                                {
                                    string dostepna = "";
                                    if (oferta.LiczbaOfert > 0)
                                        dostepna = "dostępna";
                                    else
                                        dostepna = "niedostępna";
                                    Console.WriteLine(oferta.RowKey + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + "-" + oferta.DniPobytu + "  (" + dostepna + ")");
                                }
                            }
                            else
                                Console.WriteLine("Nie znaleziono ofert.");
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D2))
                {
                    while (true)
                    {
                        MenuKlient();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj Imie: ");
                            String imie = Console.ReadLine();
                            Console.WriteLine("Podaj Nazwisko: ");
                            String nazwisko = Console.ReadLine();
                            Klient klient = new Klient(Guid.NewGuid().ToString(), imie, nazwisko);
                            if (klientRepo.Save(klient) == true)
                                Console.WriteLine("Zapisano !");
                            else
                                Console.WriteLine("Błąd zapisu");
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient klient in klienci)
                            {
                                Console.WriteLine(klient.RowKey + " --- " + klient.Imie + " - " + klient.Nazwisko);
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            Console.WriteLine("Podaj ID klienta do edycji: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kl in klienci)
                            {
                                Console.WriteLine(kl.RowKey + " --- " + kl.Imie + " - " + kl.Nazwisko);
                            }
                            String id = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(id);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                Console.WriteLine("Podawaj tylko dane które chcesz zmienić: ");
                                Console.WriteLine("Podaj imie: ");
                                String imie = Console.ReadLine();
                                Console.WriteLine("Podaj nazwisko: ");
                                String nazwisko = Console.ReadLine();
                                if(imie != "")
                                {
                                    klient.Imie = imie;
                                }
                                if(nazwisko != "")
                                {
                                    klient.Nazwisko = nazwisko;
                                }
                                //Klient klient = new Klient(Guid.NewGuid().ToString(), imie, nazwisko);
                                if (klientRepo.Update(klient) == true)
                                    Console.WriteLine("Zapisano !");
                                else
                                    Console.WriteLine("Błąd zapisu");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D4))
                        {
                            Console.WriteLine("Podaj ID klienta do usuniecia: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient klient in klienci)
                            {
                                Console.WriteLine(klient.RowKey + " --- " + klient.Imie + " - " + klient.Nazwisko);
                            }
                            String id = Console.ReadLine();
                            if (klientRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                if (klientRepo.Remove(id) == true)
                                    Console.WriteLine("Usunięto !");
                                else
                                    Console.WriteLine("Błąd usuwania");
                            }
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D3))
                {
                    while (true)
                    {
                        MenuWypozyczenia();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj ID klienta który kupuje oferte: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kli in klienci)
                            {
                                Console.WriteLine(kli.RowKey + " --- " + kli.Imie + " - " + kli.Nazwisko);
                            }
                            String klientId = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(klientId);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                Console.WriteLine("Podaj id oferty do sprzedania");
                                List<Oferta> oferty = ofertaKlientRepo.FindAllAvailable();
                                if (oferty.Count == 0)
                                {
                                    Console.WriteLine("Brak ofert");
                                    continue;
                                }
                                foreach (Oferta el in oferty)
                                {
                                    string dostepna = "";
                                    if (el.LiczbaOfert > 0)
                                        dostepna = "dostępna";
                                    else
                                        dostepna = "niedostępna";
                                    Console.WriteLine(el.RowKey + " --- " + el.Nazwa + " - " + el.Panstwo + " - " + el.Miejscowosc + "-" + el.DniPobytu + " (" + dostepna + ") " + el.LiczbaOfert);
                                }
                                String ofertaId = Console.ReadLine();
                                Oferta oferta = ofertaRepo.FindOne(ofertaId);
                                if (oferta == null)
                                {
                                    Console.WriteLine("Błędny numer ID");
                                    continue;
                                }
                                Console.WriteLine("Podaj date przybycia (w formacie mm/dd/yyyy) ");
                                String data = Console.ReadLine();
                                OfertaKlient sprzedaz = new OfertaKlient(Guid.NewGuid().ToString(), ofertaId, klientId, DateTime.Parse(data), DateTime.Parse(data).AddDays(oferta.DniPobytu));
                                if (ofertaKlientRepo.Save(sprzedaz, oferta) == true)
                                    Console.WriteLine("Sprzedano !");
                                else
                                    Console.WriteLine("Błąd sprzedaży");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            Console.WriteLine("Podaj ID klienta: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kli in klienci)
                            {
                                Console.WriteLine(kli.RowKey + " --- " + kli.Imie + " - " + kli.Nazwisko);
                            }
                            String klientId = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(klientId);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                List<OfertaKlient> sprzedaze = ofertaKlientRepo.FindAll(klientId);
                                Console.WriteLine("Posiadane oferty " + klient.Imie + " - " + klient.Nazwisko + " :");
                                foreach (OfertaKlient sprzedaz in sprzedaze)
                                {
                                    Oferta sprzedana = ofertaRepo.FindOne(sprzedaz.IdOferta.ToString());
                                    if (sprzedaz.Zakonczenie.CompareTo(DateTime.Now) > 0)
                                    {
                                        Console.WriteLine("[Niezrealizowane] --- " + sprzedana.Nazwa + " - " + sprzedana.Panstwo + " - " + sprzedana.Miejscowosc + " - " + sprzedana.DniPobytu);
                                    }
                                    else
                                    {
                                        Console.WriteLine(sprzedana.Nazwa + " - " + sprzedana.Panstwo + " - " + sprzedana.Miejscowosc + " - " + sprzedana.DniPobytu);
                                    }
                                }
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            List<Oferta> oferty = ofertaKlientRepo.FindAllAvailable();
                            if (oferty.Count == 0)
                            {
                                Console.WriteLine("Brak ofert");
                            }
                            foreach (Oferta oferta in oferty)
                            {
                                Console.WriteLine(oferta.RowKey + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + "-" + oferta.DniPobytu + "-" + oferta.LiczbaOfert);
                            }
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D4))
                {
                    inicjalizacja.DropTables(tableClient, "Oferta");
                    inicjalizacja.DropTables(tableClient, "Klient");
                    inicjalizacja.DropTables(tableClient, "OfertaKlient");
                }
                else if (key.Equals(ConsoleKey.Q))
                {
                    break;
                }
            }
        }
    }
}
