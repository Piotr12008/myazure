﻿using Microsoft.Azure.Cosmos.Table;
using MyAzureDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAzureDB.Repository
{
    public class OfertaRepo
    {
        public CloudTable Table;

        public OfertaRepo(CloudTableClient tableClient)
        {
            this.Table = tableClient.GetTableReference("Oferta");
        }
        public List<Oferta> FindAll()
        {
            TableContinuationToken token = null;
            var oferty = new List<Oferta>();
            do
            {
                var queryResult = Table.ExecuteQuerySegmented(new TableQuery<Oferta>(), token);
                oferty.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            return oferty;
        }
        public Oferta FindOne(String id)
        {
            TableOperation retrieveOperation = TableOperation.Retrieve<Oferta>(id, id);
            TableResult result = Table.Execute(retrieveOperation);
            Oferta oferta = result.Result as Oferta;

            return oferta;
        }
        public List<Oferta> FindAll(String panstwo)
        {
            TableContinuationToken token = null;
            var oferty = new List<Oferta>();
            do
            {
                var queryResult = Table.ExecuteQuerySegmented(new TableQuery<Oferta>(), token);
                oferty.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            oferty = oferty.Where(o => o.Panstwo.Equals(panstwo)).ToList();

            return oferty;
        }

        public bool Save(Oferta oferta)
        {
            if (oferta == null)
            {
                throw new ArgumentNullException("oferta");
            }
            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge((ITableEntity)oferta);
                TableResult result = Table.Execute(insertOrMergeOperation);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool Remove(String id)
        {
            try
            {
                Oferta oferta = FindOne(id);
                TableOperation deleteOperation = TableOperation.Delete(oferta);
                TableResult result = Table.Execute(deleteOperation);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
