﻿using Microsoft.Azure.Cosmos.Table;
using MyAzureDB.Entity;
using System;
using System.Collections.Generic;

namespace MyAzureDB.Repository
{
    public class KlientRepo
    {
        public CloudTable Table;

        public KlientRepo(CloudTableClient tableClient)
        {
            this.Table = tableClient.GetTableReference("Klient");
        }
        public List<Klient> FindAll()
        {
            TableContinuationToken token = null;
            var klienci = new List<Klient>();
            do
            {
                var queryResult = Table.ExecuteQuerySegmented(new TableQuery<Klient>(), token);
                klienci.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            return klienci;
        }

        public Klient FindOne(String id)
        {
            TableOperation retrieveOperation = TableOperation.Retrieve<Klient>(id, id);
            TableResult result = Table.Execute(retrieveOperation);
            Klient klient = result.Result as Klient;

            return klient;
        }

        public bool Save(Klient klient)
        {
            if (klient == null)
            {
                throw new ArgumentNullException("klient");
            }
            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge((ITableEntity)klient);
                TableResult result = Table.Execute(insertOrMergeOperation);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        public bool Update(Klient klient)
        {
            if (klient == null)
            {
                throw new ArgumentNullException("klient");
            }
            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge((ITableEntity)klient);
                TableResult result = Table.Execute(insertOrMergeOperation);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool Remove(String id)
        {
            try
            {
                Klient klient = FindOne(id);
                TableOperation deleteOperation = TableOperation.Delete(klient);
                TableResult result = Table.Execute(deleteOperation);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
