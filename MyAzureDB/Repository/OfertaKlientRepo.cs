﻿using Microsoft.Azure.Cosmos.Table;
using MyAzureDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAzureDB.Repository
{
    public class OfertaKlientRepo
    {
        public CloudTable Table;
        public CloudTableClient TableClient;

        public OfertaKlientRepo(CloudTableClient tableClient)
        {
            this.Table = tableClient.GetTableReference("OfertaKlient");
            this.TableClient = tableClient;
        }

        public List<OfertaKlient> FindAll(String id)
        {
            TableContinuationToken token = null;
            var ofertyList = new List<OfertaKlient>();
            do
            {
                var queryResult = Table.ExecuteQuerySegmented(new TableQuery<OfertaKlient>(), token);
                ofertyList.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            ofertyList = ofertyList.Where(o => o.IdKlient == id).ToList();
            return ofertyList;

        }
        public List<Oferta> FindAllAvailable()
        {
            OfertaRepo ofertaRepo = new OfertaRepo(TableClient);
            List<Oferta> oferty = ofertaRepo.FindAll();

            List<Oferta> ofertyList = oferty.Where(o => o.LiczbaOfert > 0).ToList();
            return ofertyList;
        }
        public bool Save(OfertaKlient ofertaK, Oferta oferta)
        {
            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge((ITableEntity)ofertaK);
                TableResult result = Table.Execute(insertOrMergeOperation);

                CloudTable tableO = TableClient.GetTableReference("Oferta");

                OfertaRepo ofertaRepo = new OfertaRepo(TableClient);
                Oferta ofertaUp = ofertaRepo.FindOne(ofertaK.IdOferta);
                ofertaUp.LiczbaOfert = ofertaUp.LiczbaOfert - 1;
                ofertaRepo.Save(ofertaUp);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
